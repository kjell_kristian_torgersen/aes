﻿using System;
using System.Security.Cryptography;

namespace aes
{
    class MainClass
    {
        public static void showArr(byte[] arr) { 
            for(int i = 0; i < arr.Length; i++) {
                Console.Write(arr[i].ToString("X2")+" ");
                if ((i & 7) == 7) Console.Write(" ");
                if ((i & 15) == 15) Console.WriteLine();
            }

        }

        public static void Main(string[] args)
        {
            Random rng = new Random();
            int bsize = 64;
            var key = new byte[16];
            var msg = new byte[bsize];
            var cipher = new byte[bsize];
            var deciphered = new byte[bsize];
            rng.NextBytes(key);
            rng.NextBytes(msg);

            AES_c aes = new AES_c(key, null);

            aes.Encrypt(cipher, msg);
            aes.Decrypt(deciphered, cipher);

            Console.WriteLine("msg: ");
            showArr(msg);
            Console.WriteLine("cipher: ");
            showArr(cipher);
            Console.WriteLine("deciphered: ");
            showArr(deciphered);

            /* var cipher2 = new byte[bsize];

             for (; ; )
             {

                 rng.NextBytes(key);
                 rng.NextBytes(msg);

                 Aes aes_n = Aes.Create();
                 aes_n.BlockSize = 128;
                 aes_n.KeySize = 128;
                 aes_n.Key = key;
                 aes_n.IV = iv;
                 aes_n.Padding = PaddingMode.Zeros;
                 aes_n.Mode = CipherMode.ECB;
                 var enc = aes_n.CreateEncryptor();
                 enc.TransformBlock(msg, 0, 16, cipher2, 0);

                 AES_c aes = new AES_c(key, iv);
                 aes.Encrypt(cipher, msg);
                 for(int i = 0; i < 16; i++) { 
                 if(cipher[i] != cipher2[i]) {
                          Console.WriteLine("cipher: ");
                          showArr(cipher);
                          Console.WriteLine("cipher2: ");
                          showArr(cipher2);
                     }
                 }

             }*/
            // Console.ReadLine();
        }
    }
}
