﻿using System;
namespace aes
{
    public partial class AES_c
    {
        byte[] state;
        byte[] expandedKeys;

        void SubBytes()
        {
            for (int i = 0; i < state.Length; i++)
            {
                state[i] = sbox[state[i]];
            }
        }

        void InvSubBytes()
        {
            for (int i = 0; i < state.Length; i++)
            {
                state[i] = isbox[state[i]];
            }
        }

        void MixColumns()
        {
            byte[] tmp = new byte[16];

            tmp[0] = (byte)(mul2[state[0]] ^ mul3[state[1]] ^ state[2] ^ state[3]);
            tmp[1] = (byte)(state[0] ^ mul2[state[1]] ^ mul3[state[2]] ^ state[3]);
            tmp[2] = (byte)(state[0] ^ state[1] ^ mul2[state[2]] ^ mul3[state[3]]);
            tmp[3] = (byte)(mul3[state[0]] ^ state[1] ^ state[2] ^ mul2[state[3]]);

            tmp[4] = (byte)(mul2[state[4]] ^ mul3[state[5]] ^ state[6] ^ state[7]);
            tmp[5] = (byte)(state[4] ^ mul2[state[5]] ^ mul3[state[6]] ^ state[7]);
            tmp[6] = (byte)(state[4] ^ state[5] ^ mul2[state[6]] ^ mul3[state[7]]);
            tmp[7] = (byte)(mul3[state[4]] ^ state[5] ^ state[6] ^ mul2[state[7]]);

            tmp[8] = (byte)(mul2[state[8]] ^ mul3[state[9]] ^ state[10] ^ state[11]);
            tmp[9] = (byte)(state[8] ^ mul2[state[9]] ^ mul3[state[10]] ^ state[11]);
            tmp[10] = (byte)(state[8] ^ state[9] ^ mul2[state[10]] ^ mul3[state[11]]);
            tmp[11] = (byte)(mul3[state[8]] ^ state[9] ^ state[10] ^ mul2[state[11]]);

            tmp[12] = (byte)(mul2[state[12]] ^ mul3[state[13]] ^ state[14] ^ state[15]);
            tmp[13] = (byte)(state[12] ^ mul2[state[13]] ^ mul3[state[14]] ^ state[15]);
            tmp[14] = (byte)(state[12] ^ state[13] ^ mul2[state[14]] ^ mul3[state[15]]);
            tmp[15] = (byte)(mul3[state[12]] ^ state[13] ^ state[14] ^ mul2[state[15]]);

            for (int i = 0; i < state.Length; i++) state[i] = tmp[i];
        }

        void InvMixColumns()
        {
            byte[] tmp = new byte[16];

            tmp[0] = (byte)(mul14[state[0]] ^ mul11[state[1]] ^ mul13[state[2]] ^ mul9[state[3]]);
            tmp[1] = (byte)(mul9[state[0]] ^ mul14[state[1]] ^ mul11[state[2]] ^ mul13[state[3]]);
            tmp[2] = (byte)(mul13[state[0]] ^ mul9[state[1]] ^ mul14[state[2]] ^ mul11[state[3]]);
            tmp[3] = (byte)(mul11[state[0]] ^ mul13[state[1]] ^ mul9[state[2]] ^ mul14[state[3]]);

            tmp[4] = (byte)(mul14[state[4]] ^ mul11[state[5]] ^ mul13[state[6]] ^ mul9[state[7]]);
            tmp[5] = (byte)(mul9[state[4]] ^ mul14[state[5]] ^ mul11[state[6]] ^ mul13[state[7]]);
            tmp[6] = (byte)(mul13[state[4]] ^ mul9[state[5]] ^ mul14[state[6]] ^ mul11[state[7]]);
            tmp[7] = (byte)(mul11[state[4]] ^ mul13[state[5]] ^ mul9[state[6]] ^ mul14[state[7]]);

            tmp[8] = (byte)(mul14[state[8]] ^ mul11[state[9]] ^ mul13[state[10]] ^ mul9[state[11]]);
            tmp[9] = (byte)(mul9[state[8]] ^ mul14[state[9]] ^ mul11[state[10]] ^ mul13[state[11]]);
            tmp[10] = (byte)(mul13[state[8]] ^ mul9[state[9]] ^ mul14[state[10]] ^ mul11[state[11]]);
            tmp[11] = (byte)(mul11[state[8]] ^ mul13[state[9]] ^ mul9[state[10]] ^ mul14[state[11]]);

            tmp[12] = (byte)(mul14[state[12]] ^ mul11[state[13]] ^ mul13[state[14]] ^ mul9[state[15]]);
            tmp[13] = (byte)(mul9[state[12]] ^ mul14[state[13]] ^ mul11[state[14]] ^ mul13[state[15]]);
            tmp[14] = (byte)(mul13[state[12]] ^ mul9[state[13]] ^ mul14[state[14]] ^ mul11[state[15]]);
            tmp[15] = (byte)(mul11[state[12]] ^ mul13[state[13]] ^ mul9[state[14]] ^ mul14[state[15]]);

            for (int i = 0; i < state.Length; i++) state[i] = tmp[i];
        }

        void ShiftRows()
        {
            byte[] tmp = new byte[16];

            tmp[0] = state[0];
            tmp[1] = state[5];
            tmp[2] = state[10];
            tmp[3] = state[15];

            tmp[4] = state[4];
            tmp[5] = state[9];
            tmp[6] = state[14];
            tmp[7] = state[3];

            tmp[8] = state[8];
            tmp[9] = state[13];
            tmp[10] = state[2];
            tmp[11] = state[7];

            tmp[12] = state[12];
            tmp[13] = state[1];
            tmp[14] = state[6];
            tmp[15] = state[11];

            for (int i = 0; i < state.Length; i++) state[i] = tmp[i];
        }

        void InvShiftRows()
        {
            byte[] tmp = new byte[16];

            tmp[0] = state[0];
            tmp[4] = state[4];
            tmp[8] = state[8];
            tmp[12] = state[12];

            tmp[1] = state[13];
            tmp[5] = state[1];
            tmp[9] = state[5];
            tmp[13] = state[9];

            tmp[2] = state[10];
            tmp[6] = state[14];
            tmp[10] = state[2];
            tmp[14] = state[6];

            tmp[3] = state[7];
            tmp[7] = state[11];
            tmp[11] = state[15];
            tmp[15] = state[3];

            for (int i = 0; i < state.Length; i++) state[i] = tmp[i];
        }

        void KeyExpansion(byte[] key)
        {
            expandedKeys = new byte[176];

            for (int i = 0; i < key.Length; i++) expandedKeys[i] = key[i];

            int bytesGenerated = 16;
            int rconIteration = 1;
            byte[] temp = new byte[4];
            while (bytesGenerated < 176)
            {
                for (int i = 0; i < 4; i++)
                {
                    temp[i] = expandedKeys[i + bytesGenerated - 4];
                }

                if (bytesGenerated % 16 == 0)
                {
                    KeyExpansionCore(temp, rconIteration);
                    rconIteration++;
                }

                for (int a = 0; a < 4; a++)
                {
                    expandedKeys[bytesGenerated] = (byte)(expandedKeys[bytesGenerated - 16] ^ temp[a]);
                    bytesGenerated++;
                }
            }
        }

        void KeyExpansionCore(byte[] key, int i)
        {
            byte t = key[0];
            key[0] = key[1];
            key[1] = key[2];
            key[2] = key[3];
            key[3] = t;

            key[0] = sbox[key[0]];
            key[1] = sbox[key[1]];
            key[2] = sbox[key[2]];
            key[3] = sbox[key[3]];

            key[0] ^= rcon[i];
        }

        void AddRoundKey(byte[] roundKey, int offset = 0)
        {
            for (int i = 0; i < state.Length; i++)
            {
                state[i] ^= roundKey[i + offset];
            }
        }

        public AES_c(byte[] key, byte[] iv)
        {
            state = new byte[16];
            KeyExpansion(key);
        }

        public void Encrypt(byte[] dest, byte[] source)
        {
            int blocks = source.Length / 16;
            for (int b = 0; b < blocks; b++)
            {
                for (int i = 0; i < state.Length; i++)
                {
                    state[i] = source[16 * b + i];
                }

                int numberOfRounds = 9;
                AddRoundKey(expandedKeys);

                for (int i = 0; i < numberOfRounds; i++)
                {
                    SubBytes();
                    ShiftRows();
                    MixColumns();
                    AddRoundKey(expandedKeys, 16 * (i + 1));
                }
                SubBytes();
                ShiftRows();
                AddRoundKey(expandedKeys, 160);

                for (int i = 0; i < 16; i++) dest[16 * b + i] = state[i];
            }
        }

        public void Decrypt(byte[] dest, byte[] source)
        {
            int blocks = source.Length / 16;
            for (int b = 0; b < blocks; b++)
            {
                for (int i = 0; i < state.Length; i++)
                {
                    state[i] = source[16 * b + i];
                }

                int numberOfRounds = 9;
                AddRoundKey(expandedKeys, 160);

                for (int i = numberOfRounds; i > 0; i--)
                {
                    InvShiftRows();
                    InvSubBytes();
                    AddRoundKey(expandedKeys, 16 * i);
                    InvMixColumns();

                }
                InvShiftRows();
                InvSubBytes();
                AddRoundKey(expandedKeys);

                for (int i = 0; i < 16; i++) dest[16 * b + i] = state[i];
            }
        }
    }
}
